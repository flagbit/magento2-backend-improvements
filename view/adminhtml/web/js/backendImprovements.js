define([
    "jquery",
    "uiComponent",
    "domReady!"
], function ($, Component) {

    return Component.extend({
        defaults: {
            inputsName: [
                'entity_id[from]',
                'entity_id[to]',
                'price[from]',
                'price[to]',
                'qty[from]',
                'qty[to]',
                'name',
                'sku'
            ],
            keyCode: 13,
            buttonSelector: 'button[data-action="grid-filter-apply"]'
        },

        initialize: function () {
            this._super();
            this.initEventListener();
        },

        initEventListener: function() {
            $(document).bind('keypress', this.onKeyPress.bind(this));
        },

        onKeyPress: function (e) {
            // keyCode 13 is ENTER
            if (e.keyCode === this.keyCode) {
                if (this.inputsName.indexOf(document.activeElement.name) != -1) {
                    var event = new Event('change');
                    document.activeElement.dispatchEvent(event);
                    $(this.buttonSelector).click();
                }
            }
        }
    });
});