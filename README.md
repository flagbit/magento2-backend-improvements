# Magento2 Backend Improvements

This module is listening for keypress ENTER 
on the Magento2 backend page catalog product (product listing grid).  

It is working in this fields:
entity_id[from], entity_id[to], price[from], price[to], 
qty[from], qty[to], sku and name.

![backend improvements image](https://bytebucket.org/flagbit/magento2-backend-improvements/raw/c44ae69d7f7cb54adfc5f672d9783ec994faf3c3/magento2-backend-improvements.jpg "Magento2 Backend Improvements")

## License

This project is licensed under the MIT License.